import unittest
from numerics import vanilla_option
from numerics.barrier_option import barrier_option_calc
from product.barrier_option import BarrierOption
from product.product_utils import *
from single_barrier_pricer import single_barrier_pricer, single_barrier_calc_vec


class TestBarrierOption(unittest.TestCase):

    def test_vanilla_option(self):
        vanilla_call = vanilla_option.price(100., 100., 0.01, 0., 0.3, 1., OptionType.Call)
        vanilla_put = vanilla_option.price(100., 100., 0.01, 0., 0.3, 1., OptionType.Put)
        self.assertAlmostEqual(vanilla_call, 12.3683, places=4)
        self.assertAlmostEqual(vanilla_put, 11.3733, places=4)

    def test_barrier_pricer(self):
        uoc = BarrierOption(strike=100, option_type=OptionType.Call, barrier=120, direction=BarrierDirectionEnum.Up,
                            barrier_type=BarrierTypeEnum.Out, observation_type=ObservationTypeEnum.Continuous)
        result = single_barrier_calc_vec(single_barrier_pricer, uoc, spot=100., r=0.01, vol=0.3,
                                         tau=1., q=0., day_count=DayCountConvention.ACT365)
        self.assertAlmostEqual(result[0], 0.42867619944545865, places=4, msg="uoc price test failed")

    def test_barrier_pricer_discrete(self):
        uoc = BarrierOption(strike=100, option_type=OptionType.Call, barrier=120, direction=BarrierDirectionEnum.Up,
                            barrier_type=BarrierTypeEnum.Out, observation_type=ObservationTypeEnum.Discrete)
        result = single_barrier_calc_vec(single_barrier_pricer, uoc, spot=100., r=0.01, vol=0.3,
                                         tau=1., q=0., day_count=DayCountConvention.BUS244)
        self.assertAlmostEqual(result[0], 0.5291434102225603, places=4, msg="uoc price test failed")

    def test_barrier_option_call_price(self):
        vanilla_call = vanilla_option.price(spot=100, strike=100, r=0.01, q=0.,
                                            vol=0.3, tau=1., option_type=OptionType.Call)
        # Test uoc price with benchmark, uoc+uic == vanilla
        uoc_price = barrier_option_calc(100, 100, 120, 0.01, 0., 0.3, 1.,
                                        OptionType.Call, BarrierTypeEnum.Out, BarrierDirectionEnum.Up)
        uic_price = barrier_option_calc(100, 100, 120, 0.01, 0., 0.3, 1.,
                                        OptionType.Call, BarrierTypeEnum.In, BarrierDirectionEnum.Up)
        self.assertAlmostEqual(uoc_price, 0.42867619944545865, places=4,
                               msg="uoc price test failed")
        self.assertAlmostEqual(uoc_price+uic_price, vanilla_call, places=4,
                               msg="knock-in knock-out parity failed, uoc + uic parity failed")

        # Test doc price with different strike and barrier relations,
        # Test doc + dic == vanilla_call
        doc_price = barrier_option_calc(100, 100, 80, 0.01, 0., 0.3, 1.,
                                        OptionType.Call, BarrierTypeEnum.Out, BarrierDirectionEnum.Down)
        dic_price = barrier_option_calc(100, 100, 80, 0.01, 0., 0.3, 1.,
                                        OptionType.Call, BarrierTypeEnum.In, BarrierDirectionEnum.Down)
        self.assertAlmostEqual(doc_price, 11.452499926095904, places=4,
                               msg="doc price test failed, strike > barrier")
        self.assertAlmostEqual(doc_price + dic_price, vanilla_call, places=4,
                               msg="dic + doc knock-in knock-out parity failed, strike > barrier")

        doc_price2 = barrier_option_calc(130, 100, 120, 0.01, 0., 0.3, 1., OptionType.Call,
                                         BarrierTypeEnum.Out, BarrierDirectionEnum.Down)
        dic_price2 = barrier_option_calc(130, 100, 120, 0.01, 0., 0.3, 1., OptionType.Call,
                                         BarrierTypeEnum.In, BarrierDirectionEnum.Down)
        vanilla_call2 = vanilla_option.price(spot=130, strike=100, vol=0.3, r=0.01, tau=1.,
                                             option_type=OptionType.Call, q=0.)
        self.assertAlmostEqual(doc_price2, 14.09861454476631, places=4,
                               msg="doc price test failed, strike < barrier")
        self.assertAlmostEqual(doc_price2 + dic_price2, vanilla_call2, places=4,
                               msg="dic+doc knock-in knock-out parity failed, strike < barrier")

    def test_barrier_option_put_price(self):
        vanilla_put = vanilla_option.price(spot=100, strike=100, r=0.01, q=0., tau=1.,
                                           vol=0.3, option_type=OptionType.Put)
        dop_price = barrier_option_calc(100, 100, 80, 0.01, 0., 0.3, 1., OptionType.Put,
                                        BarrierTypeEnum.Out, BarrierDirectionEnum.Down)
        dip_price = barrier_option_calc(100, 100, 80, 0.01, 0., 0.3, 1., OptionType.Put,
                                        BarrierTypeEnum.In, BarrierDirectionEnum.Down)
        self.assertAlmostEqual(dop_price, 0.841897917903939, places=4, msg="dop price test failed")
        self.assertAlmostEqual(dop_price + dip_price, vanilla_put, places=6, msg='dop parity failed')

        uop_price1 = barrier_option_calc(100, 100, 120, 0.01, 0., 0.3, 1., OptionType.Put,
                                         BarrierTypeEnum.Out, BarrierDirectionEnum.Up)
        uip_price1 = barrier_option_calc(100, 100, 120, 0.01, 0., 0.3, 1., OptionType.Put,
                                         BarrierTypeEnum.In, BarrierDirectionEnum.Up)
        self.assertAlmostEqual(uop_price1, 9.808919188397802, places=4,
                               msg="uop price test failed, strike < barrier")
        self.assertAlmostEqual(uop_price1 + uip_price1, vanilla_put, places=6,
                               msg="knock-in knock-out parity failed, strike < barrier")

        vanilla_put2 = vanilla_option.price(spot=80, strike=100, vol=0.3, r=0.01, tau=1.,
                                            q=0., option_type=OptionType.Put)
        uop_price2 = barrier_option_calc(80, 100, 90, 0.01, 0., 0.3, 1., OptionType.Put,
                                         BarrierTypeEnum.Out, BarrierDirectionEnum.Up)
        uip_price2 = barrier_option_calc(80, 100, 90, 0.01, 0., 0.3, 1., OptionType.Put,
                                         BarrierTypeEnum.In, BarrierDirectionEnum.Up)
        self.assertAlmostEqual(uop_price2, 12.884385677075894, places=4,
                               msg="uop price test failed, strike > barrier")
        self.assertAlmostEqual(uop_price2 + uip_price2, vanilla_put2, places=6,
                               msg="knock-in knock-out parity failed, strike > barrier")
