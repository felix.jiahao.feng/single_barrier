from enum import Enum


class BarrierDirectionEnum(Enum):
    Up = "Up"
    Down = "Down"


class BarrierTypeEnum(Enum):
    Out = "Out"
    In = "In"


class DayCountConvention(Enum):
    ACT365 = 365
    BUS244 = 244


class ObservationTypeEnum(Enum):
    Continuous = 1
    Discrete = 2
    Terminal = 3


class OptionType(Enum):
    Call = "Call"
    Put = "Put"


class PaymentType(Enum):
    Expiry = "Expiry"
    Instant = "Instant"


class ProductEnum(Enum):
    VanillaOption = "VanillaOption"
    SingleBarrierOption = "SingleBarrierOption"


class UnderlyingType(Enum):
    EquityStock = "EquityStock"
    EquityIndex = "EquityIndex"
    Commodity = "Commodity"
