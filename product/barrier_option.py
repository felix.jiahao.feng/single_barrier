import math
from product.product_utils import OptionType, BarrierTypeEnum, PaymentType, BarrierDirectionEnum, DayCountConvention, \
    ObservationTypeEnum


class BarrierOption(object):
    def __init__(self,
                 strike: float,
                 option_type: OptionType,
                 barrier: float,
                 direction: BarrierDirectionEnum,
                 barrier_type: BarrierTypeEnum,
                 observation_type: ObservationTypeEnum,
                 has_knocked=False,
                 rebate=0.,
                 rebate_type=PaymentType.Expiry):
        self.strike_ = strike
        self.option_type_ = option_type
        self.barrier_ = barrier
        self.direction_ = direction
        self.barrier_type_ = barrier_type
        self.observation_type_ = observation_type
        self.rebate_ = rebate
        self.rebate_type_ = rebate_type
        self.has_knocked_ = has_knocked

    def direction(self):
        return self.direction_

    def barrier_type_(self):
        return self.barrier_type_

    def strike(self):
        return self.strike_

    def option_type(self):
        return self.option_type_

    def barrier(self, vol, day_count):
        if self.observation_type_ == ObservationTypeEnum.Continuous:
            return self.barrier_
        elif self.observation_type_ == ObservationTypeEnum.Discrete:
            adjusted_factor = vol * math.sqrt(1. / day_count.value) * 0.582597159390107
            return self.barrier_ * math.exp(adjusted_factor) \
                if self.direction_ == BarrierDirectionEnum.Up else self.barrier_ * math.exp(-adjusted_factor)

    def barrier_type(self):
        return self.barrier_type_

    def rebate(self):
        return self.rebate_

    def rebate_type(self):
        return self.rebate_type_

    def observation_type(self):
        return self.observation_type_

    def is_knock_in(self, spot: float):
        if self.barrier_type_ == BarrierTypeEnum.Out:
            return False
        else:
            if self.direction_ == BarrierDirectionEnum.Up:
                if spot > self.barrier_:
                    return True
                else:
                    return False
            else:
                if spot < self.barrier_:
                    return True
                else:
                    return False

    def is_knocked_out(self, spot: float):
        if self.barrier_type_ == BarrierTypeEnum.In:
            return False
        else:
            if self.direction_ == BarrierDirectionEnum.Up:
                if spot > self.barrier_:
                    return True
                else:
                    return False
            else:
                if spot < self.barrier_:
                    return True
                else:
                    return False

    def payoff(self, spot):
        if self.direction_ == BarrierDirectionEnum.Up:
            if self.barrier_type_ == BarrierTypeEnum.Out:
                if spot >= self.barrier_:
                    return self.rebate_
                else:
                    if self.option_type_ == OptionType.Call:
                        return max(spot - self.strike_, 0.)
                    else:
                        return max(self.strike_ - spot, 0.)

            else:
                if spot < self.barrier_:
                    return self.rebate_
                else:
                    if self.option_type_ == OptionType.Call:
                        if self.option_type_ == OptionType.Call:
                            return max(spot - self.strike_, 0.)
                        else:
                            return max(self.strike_ - spot, 0.)

        else:
            if self.barrier_type_ == BarrierTypeEnum.Out:
                if spot <= self.barrier_:
                    return self.rebate_
                else:
                    if self.option_type_ == OptionType.Call:
                        return max(spot - self.strike_, 0.)
                    else:
                        return max(self.strike_ - spot, 0.)

            else:
                if spot > self.barrier_:
                    return self.rebate_
                else:
                    if self.option_type_ == OptionType.Call:
                        return max(spot - self.strike_, 0.)
                    else:
                        return max(self.strike_ - spot, 0.)

