from numerics.barrier_option import barrier_option_calc
from product.barrier_option import BarrierOption
from product.product_utils import DayCountConvention, BarrierTypeEnum, OptionType, BarrierDirectionEnum, \
    ObservationTypeEnum


def single_barrier_pricer(
        instrument: BarrierOption,
        spot: float,
        r: float,
        vol: float,
        tau: float,
        q: float,
        day_count: DayCountConvention,
) -> float:
    if tau == 0.:
        return instrument.payoff(spot)
    if instrument.barrier_type() == BarrierTypeEnum.In:
        if not instrument.is_knock_in(spot):
            return 0.
    elif instrument.barrier_type() == BarrierTypeEnum.Out:
        if instrument.is_knocked_out(spot):
            return instrument.rebate()

    strike = instrument.strike()
    barrier = instrument.barrier(vol, day_count)
    option_type = instrument.option_type()
    barrier_type = instrument.barrier_type()
    direction = instrument.direction()
    return barrier_option_calc(spot, strike, barrier, r, q, vol, tau, option_type, barrier_type, direction)

        # price_spot_up = barrier_option_calc(spot + dx, strike, barrier, r, q, vol, tau, option_type, barrier_type,
        # direction) price_spot_down = barrier_option_calc(spot - dx, strike, barrier, r, q, vol, tau, option_type,
        # barrier_type, direction) price_vol_up = barrier_option_calc(spot, strike, barrier, r, q, vol + dvol, tau,
        # option_type, barrier_type, direction)

        # delta = (price_spot_up - price_spot_down) / (2 * dx)
        # gamma = (price_spot_up + price_spot_down - 2 * price) / (dx * dx)
        # vega = (price_vol_up - price) / dvol


def single_barrier_calc_vec(
        instrument: BarrierOption,
        spot: float,
        r: float,
        vol: float,
        tau: float,
        q: float,
        day_count: DayCountConvention,
):
    pricer = single_barrier_pricer
    price_ = pricer(instrument, spot, r, vol, tau, q, day_count)
    if tau == 0.:
        return [price_, price_, 0., 0.]
    dx = spot * 1e-5
    dvol = 1e-6
    price_spot_up = pricer(instrument, spot + dx, r, vol, tau, q, day_count)
    price_spot_down = pricer(instrument, spot - dx, r, vol, tau, q, day_count)
    price_vol_up = pricer(instrument, spot, r, vol + dvol, tau, q, day_count)

    delta = (price_spot_up - price_spot_down) / (2 * dx)
    gamma = (price_spot_up + price_spot_down - 2 * price_) / (dx ** 2)
    vega = (price_vol_up - price_) / dvol
    return [price_, delta, gamma, vega]


if __name__ == "__main__":
    barrier_continuous = BarrierOption(100, OptionType.Call, 130, BarrierDirectionEnum.Up, BarrierTypeEnum.Out,
                                       ObservationTypeEnum.Continuous)
    price = single_barrier_calc_vec(barrier_continuous, 100, 0.01, 0.3, 1., 0., DayCountConvention.ACT365)
