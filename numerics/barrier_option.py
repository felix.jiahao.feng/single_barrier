import math
from product.product_utils import BarrierTypeEnum, BarrierDirectionEnum, OptionType
from scipy.special import ndtr


def a_calc(barrier, spot, r, q, vol):
    return (barrier / spot) ** (-1 + (2 * (r - q) / vol ** 2))


def b_calc(barrier, spot, r, q, vol):
    return (barrier / spot) ** (1 + (2 * (r - q) / vol ** 2))


def d1_calc(spot, strike, r, q, vol, tau):
    return (math.log(spot / strike) + (r - q + .5 * vol ** 2) * tau) / (vol * math.sqrt(tau))


def d2_calc(spot, strike, r, q, vol, tau):
    return (math.log(spot / strike) + (r - q - .5 * vol ** 2) * tau) / (vol * math.sqrt(tau))


def d3_calc(spot, barrier, r, q, vol, tau):
    return (math.log(spot / barrier) + (r - q + .5 * vol ** 2) * tau) / (vol * math.sqrt(tau))


def d4_calc(spot, barrier, r, q, vol, tau):
    return (math.log(spot / barrier) + (r - q - .5 * vol ** 2) * tau) / (vol * math.sqrt(tau))


def d5_calc(spot, barrier, r, q, vol, tau):
    return (math.log(spot / barrier) - (r - q - .5 * vol ** 2) * tau) / (vol * math.sqrt(tau))


def d6_calc(spot, barrier, r, q, vol, tau):
    return (math.log(spot / barrier) - (r - q + .5 * vol ** 2) * tau) / (vol * math.sqrt(tau))


def d7_calc(spot, strike, barrier, r, q, vol, tau):
    return (math.log(spot * strike / barrier ** 2) - (r - q - .5 * vol ** 2) * tau) / (vol * math.sqrt(tau))


def d8_calc(spot, strike, barrier, r, q, vol, tau):
    return (math.log(spot * strike / barrier ** 2) - (r - q + .5 * vol ** 2) * tau) / (vol * math.sqrt(tau))


def barrier_option_calc(spot, strike, barrier, r, q, vol, tau, option_type: OptionType,
                        barrier_type: BarrierTypeEnum, barrier_direction: BarrierDirectionEnum) -> float:
    a = a_calc(barrier, spot, r, q, vol)
    b = b_calc(barrier, spot, r, q, vol)
    d1 = d1_calc(spot, strike, r, q, vol, tau)
    d2 = d2_calc(spot, strike, r, q, vol, tau)
    d3 = d3_calc(spot, barrier, r, q, vol, tau)
    d4 = d4_calc(spot, barrier, r, q, vol, tau)
    d5 = d5_calc(spot, barrier, r, q, vol, tau)
    d6 = d6_calc(spot, barrier, r, q, vol, tau)
    d7 = d7_calc(spot, strike, barrier, r, q, vol, tau)
    d8 = d8_calc(spot, strike, barrier, r, q, vol, tau)
    if barrier_direction == BarrierDirectionEnum.Up:

        if barrier_type == BarrierTypeEnum.Out:

            if option_type == OptionType.Call:
                # Se−q(T −t) (N(d1) − N(d3) − b(N(d6) − N(d8)))
                # − Ee−r(T −t) (N(d2) − N(d4) − a(N(d5) − N(d7))) .
                return spot * math.exp(-q * tau) * (ndtr(d1) - ndtr(d3) - b * (ndtr(d6) - ndtr(d8))) - \
                    strike * math.exp(-r * tau) * (ndtr(d2) - ndtr(d4) - a * (ndtr(d5) - ndtr(d7)))

            elif option_type == OptionType.Put:

                if strike > barrier:
                    # −Se−q(T −t) (1 − N(d3) − bN(d6)) + Ee−r(T −t) (1 − N(d4) − aN(d5))
                    return -spot * math.exp(-q * tau) * (1 - ndtr(d3) - b * ndtr(d6)) + \
                        strike * math.exp(-r * tau) * (1 - ndtr(d4) - a * ndtr(d5))

                elif strike < barrier:
                    # −Se−q(T −t) (1 − N(d1) − bN(d8)) + Ee−r(T −t) (1 − N(d2) − aN(d7))
                    return -spot * math.exp(-q * tau) * (1 - ndtr(d1) - b * ndtr(d8)) + \
                        strike * math.exp(-r * tau) * (1 - ndtr(d2) - a * ndtr(d7))

        elif barrier_type == BarrierTypeEnum.In:

            if option_type == OptionType.Call:
                # Se−q(T −t) (N (d3) + b(N (d6) − N (d8))) − Ee−r(T −t) (N (d4) + a(N (d5) − N (d7)))
                return spot * math.exp(-q * tau) * (ndtr(d3) + b * (ndtr(d6) - ndtr(d8))) - \
                    strike * math.exp(-r * tau) * (ndtr(d4) + a * (ndtr(d5) - ndtr(d7)))

            elif option_type == OptionType.Put:

                if strike > barrier:
                    # −Se−q(T −t) (N (d3) − N (d1) + bN (d6)) + Ee−r(T −t) (N (d4) − N (d2) + aN (d5))
                    return -spot * math.exp(-q * tau) * (ndtr(d3) - ndtr(d1) + b * ndtr(d6)) + \
                        strike * math.exp(-r * tau) * (ndtr(d4) - ndtr(d2) + a * ndtr(d5))

                elif strike < barrier:
                    # −Se−q(T −t)bN(d8) + Ee−r(T −t)aN(d7)
                    return -spot * math.exp(-q * tau) * b * ndtr(d8) + strike * math.exp(-r * tau) * a * ndtr(d7)

    elif barrier_direction == BarrierDirectionEnum.Down:

        if barrier_type == BarrierTypeEnum.Out:

            if option_type == OptionType.Call:

                if strike > barrier:
                    # Se−q(T −t) (N (d1) − b(1 − N (d8))) − Ee−r(T −t) (N (d2) − a(1 − N (d7)))
                    return spot * math.exp(-q * tau) * (ndtr(d1) - b * (1 - ndtr(d8))) -\
                        strike * math.exp(-r * tau) * (ndtr(d2) - a * (1 - ndtr(d7)))

                elif strike < barrier:
                    # Se−q(T −t) (N (d3) − b(1 − N (d6))) − Ee−r(T −t) (N (d4) − a(1 − N (d5)))
                    return spot * math.exp(-q * tau) * (ndtr(d3) - b * (1 - ndtr(d6))) - \
                           strike * math.exp(-r * tau) * (ndtr(d4) - a * (1 - ndtr(d5)))

            elif option_type == OptionType.Put:
                # −Se−q(T −t) (N (d3) − N (d1) − b(N (d8) − N (d6)))
                # + Ee−r(T −t) (N (d4) − N (d2) − a(N (d7) − N (d5)))
                return -spot * math.exp(-q * tau) * (ndtr(d3) - ndtr(d1) - b * (ndtr(d8) - ndtr(d6))) +\
                    strike * math.exp(-r * tau) * (ndtr(d4) - ndtr(d2) - a * (ndtr(d7) - ndtr(d5)))

        elif barrier_type == BarrierTypeEnum.In:

            if option_type == OptionType.Call:

                if strike > barrier:
                    # Se−q(T −t)b(1 − N (d8)) − Ee−r(T −t)a(1 − N (d7))
                    return spot * math.exp(-q * tau) * b * (1 - ndtr(d8)) -\
                        strike * math.exp(-r * tau) * a * (1 - ndtr(d7))

                elif strike < barrier:
                    # Se−q(T −t) (N (d1) − N (d3) + b(1 − N (d6)))
                    # − Ee−r(T −t) (N (d2) − N (d4) + a(1 − N (d5)))
                    return spot * math.exp(-q * tau) * (ndtr(d1) - ndtr(d3) + b * (1 - ndtr(d6))) -\
                        strike * math.exp(-r * tau) * (ndtr(d2) - ndtr(d4) + a * (1 - ndtr(d5)))

            elif option_type == OptionType.Put:
                # −Se−q(T −t) (1 − N (d3) + b(N (d8) − N (d6)))
                # + Ee−r(T −t) (1 − N (d4) + a(N (d7) − N (d5)))
                return -spot * math.exp(-q * tau) * (1 - ndtr(d3) + b * (ndtr(d8) - ndtr(d6))) +\
                    strike * math.exp(-r * tau) * (1 - ndtr(d4) + a * (ndtr(d7) - ndtr(d5)))
