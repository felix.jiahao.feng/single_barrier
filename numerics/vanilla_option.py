import math
from scipy.special import ndtr
from product.product_utils import OptionType


def d1_calc(spot, strike, r, q, vol, tau):
    return (math.log(spot / strike) + (r - q + .5 * vol ** 2) * tau) / (vol * math.sqrt(tau))


def d2_calc(spot, strike, r, q, vol, tau):
    return (math.log(spot / strike) + (r - q - .5 * vol ** 2) * tau) / (vol * math.sqrt(tau))


def price(spot, strike, r, q, vol, tau, option_type: OptionType):
    d1 = d1_calc(spot, strike, r, q, vol, tau)
    d2 = d2_calc(spot, strike, r, q, vol, tau)
    if option_type == OptionType.Call:
        return spot * math.exp(-q * tau) * ndtr(d1) - strike * math.exp(-r * tau) * ndtr(d2)
    else:
        return -spot * math.exp(-q * tau) * ndtr(-d1) + strike * math.exp(-r * tau) * ndtr(-d2)
